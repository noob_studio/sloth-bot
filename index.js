require('dotenv').config(); //initialize dotenv
var fs = require('fs');
var exam = require('./example/test.json')
var toValidate = []
const Discord = require('discord.js'); //import discord.js
const Intents = Discord.Intents;
const Embed = Discord.MessageEmbed;
const MessageAttachment = Discord.MessageAttachment;
const client = new Discord.Client({intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MEMBERS,Intents.FLAGS.GUILD_MESSAGES] }); //create new client

var getExam = function(){
  let _index = Math.floor(Math.random() * exam.length);
  let _exam = exam[_index]
  _exam.choiceString = function(){
    let optionString = "";
    for(let i=0;i<_exam.choice.length;i++){
      let x = i+1;
      optionString += `${x}. ${_exam.choice[i]} \n`
    }
    return optionString;
  }
  return _exam
}

var addValidate = function(id, timeout, _exam){
  toValidate.push({ id, timeout, exam: _exam })
}
client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('guildMemberAdd' , member => {
  let _exam = getExam();
  const file = new MessageAttachment(_exam.image);
  member.guild.channels.cache.get('884797055900450896').send(`
Heyyyyy ${member} what is this Image dude? \n 
${_exam.choiceString()}
Please awser in one minute.`)
  member.guild.channels.cache.get('884797055900450896').send({ files: [file] })
  let timeout = setTimeout(() => { 
    member.guild.channels.cache.get('884797055900450896').send(`To late dude ${member}, bye..`);
    member.kick()
  }, 60000); // 1 minute
  addValidate(member.user.id, timeout, _exam)
})

client.on("message", message => {
  if (message.author.bot) return;
  if (message.author.system) return;
  const findId = (row) => row.id == message.author.id
  let _toValidate = toValidate.find(findId)
  let index = toValidate.findIndex(findId)
  if(!_toValidate) return;
  console.log(JSON.stringify(_toValidate.exam.choice))
  console.log(_toValidate.exam.correct)
  console.log(message.content)
  if(message.content.toUpperCase() != _toValidate.exam.choice[_toValidate.exam.correct].toUpperCase()){
    message.reply('not correct dude.')
  }else{
    clearTimeout(_toValidate.timeout);
    toValidate.splice(index, 1)
    message.reply('is correct dude. now you are our member.')
    let role = message.guild.roles.cache.find(r => r.id === "884479904928903218");
    message.guild.members.cache.get(message.author.id).roles.add(role);
  }
})

client.login(process.env.CLIENT_TOKEN); 


// console.log(JSON.stringify(_exam))